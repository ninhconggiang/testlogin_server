const express = require('express');
const https = require('https');
const { OAuth2Client } = require('google-auth-library');
const CLIENT_ID = "717324258916-t4p195vd82u7jrshhvpqi7o9tdnlj05o.apps.googleusercontent.com"
const client = new OAuth2Client(CLIENT_ID);// Replace by your client ID
const app = express();
const port = 8080;
let cors = require("cors");
app.use(cors());
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.post('/', function (req, res) {

    console.log("req", req.body.data)
    //xác minh người dùng từ client gửi lên 
    if (req.body?.data?.graphDomain == "facebook") {
        //xác minh người dùng với facebook
        const options = {
            hostname: 'graph.facebook.com',
            path: '/me?access_token=' + req.body.data.accessToken,
            method: 'GET'
        }

        const request = https.get(options, response => {
            response.on('data', function (user) {
                user = JSON.parse(user.toString());
                console.log(user);
                if (user.id === req.body.data.id) {
                    console.log("xác minh thành công")
                }
            });
        })

        request.on('error', (message) => {
            console.error(message);
        });

        request.end();
    } else {
        // xác minh người dùng với gg
        async function verify() {
            try {
                const ticket = await client.verifyIdToken({
                    idToken: req.body.data.tokenObj.id_token,
                    audience: CLIENT_ID,
                });
                const payload = ticket.getPayload();
                console.log(payload)
                if (payload.sub === req.body.data.profileObj.googleId) {
                    console.log("xác minh thành công")
                }
            } catch (error) {
                console.log(error)
            }
        }
        verify()
    }

    res.send("Hello Worldsss");
})
app.listen(port, function () {
    console.log("Your app running on port " + port);
})